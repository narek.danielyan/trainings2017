#include <iostream>

int 
main()
{
    while (true) {
        int accountNumber;
        std::cout << "Enter account number (-1 to end): ";
        std::cin >> accountNumber;
        if (-1 == accountNumber) {
            break;
        }

        double oldBalance;
        std::cout << "Enter beginning balance: ";
        std::cin >> oldBalance;

        double charges;
        std::cout << "Enter total charges: ";
        std::cin >> charges;

        double credits;
        std::cout << "Enter total credits: ";
        std::cin >> credits;

        double creditLimit;
        std::cout << "Enter credit limit: ";
        std::cin >> creditLimit;

        double newBalance = oldBalance + charges - credits;
        std::cout << "New balance: " << newBalance << "\n\n";

        if (newBalance > creditLimit) {
            std::cout << "Account: " << accountNumber << std::endl;
            std::cout << "Credit limit: " << creditLimit << std::endl;
            std::cout << "Balance:    " << newBalance << std::endl;
            std::cout << "Credit is Exceeded.\n\n";
        }
    }

    return 0;
}
