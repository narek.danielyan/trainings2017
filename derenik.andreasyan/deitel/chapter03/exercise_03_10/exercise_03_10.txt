Recall that declaring data members with access specifier private enforces data hiding. Providing public set and get functions allows clients of a class to access the
hidden data, but only indirectly. The client knows that it is attempting to modify or obtain an object's data, but the client does not know how the object performs
these operations. In some cases, a class may internally represent a piece of data one way, but expose that data to clients in a different way.
