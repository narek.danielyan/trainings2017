a)
if (age >= 65){   
    std::cout << "Age above or egual 65" << std::endl;
} else {
    std::cout << "Age below 65" << std::endl;
}

b)
if (age >= 65){
    std::cout << "Age above or egual 65" << std::endl;
} else {
    std::cout << "Age below 65" << std::endl;
}

c)
int x = 1, total = 0;
    while (x <= 10){
        total += x;
        std::cout << "Total is " << total << std::endl; 
        ++x;
        std::cout << "X is " << x << std::endl;
    } 

d)
int x = 0, total = 0;
while (x <= 100){
    total += x;
    std::cout << "Total is " << total << std::endl; 
    ++x;
    std::cout << "X is " << x << std::endl;
} 

e)
int y = 1;
while (y < 10){
    std::cout << "Y is " << y << std::endl;
    y++;
} 

