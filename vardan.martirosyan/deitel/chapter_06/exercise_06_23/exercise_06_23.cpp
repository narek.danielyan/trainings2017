#include <iostream>
#include <cassert>

void
square(const int side, const char fillCharacter)
{
    assert(side > 0);
    for (int i = 1; i <= side; ++i) {
        for (int j = 1; j <= side; ++j) {
            std::cout << fillCharacter;
        }
        std::cout << std::endl;
    }
}

int 
main()
{
    int number;
    std::cout << "Enter number: ";
    std::cin  >> number;

    if(number <= 0) {
        std::cerr << "Error 1: number can not be negative. " << std::endl;
        return 1;
    }
    char fillCharacter;
    std::cout << "Enter symbol: ";
    std::cin  >> fillCharacter;

    square(number, fillCharacter);
    return 0;
}
