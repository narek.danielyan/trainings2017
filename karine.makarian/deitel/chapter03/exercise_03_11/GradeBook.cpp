#include <iostream>
#include "GradeBook.hpp"

GradeBook::GradeBook(std::string name, std::string instructorName) 
{
    setCourseName(name);
    setInstructorName(instructorName);
}

void
GradeBook::setCourseName(std::string name)
{
    courseName_ = name;
}

std::string
GradeBook::getCourseName() 
{
    return courseName_;
}

void
GradeBook::displayMessage() 
{
    std::cout << "Welcome to the grade book for " << getCourseName() << std::endl;
    std::cout << "Course is presented by " << getInstructorName() << std::endl;
}

void
GradeBook::setInstructorName(std::string instructorName)
{
    instructorName_ = instructorName; 
}

std::string
GradeBook::getInstructorName()
{
    return instructorName_;
}

