#include <iostream>

int
main()
{
    
    for (int i = 1; i <= 5; ++i) {

        int number;
        std::cout << "Enter the  number (from 1 to 30): ";
        std::cin >> number;

        if (number < 1 || number > 30) {
            std::cerr << "Error 1: Invalid number" << std::endl;
            return 1;
        }

        for (int j = 1; j <= number; ++j) {
            std::cout << "*";
        }

        std::cout << std::endl;
    }

    return 0;
}

