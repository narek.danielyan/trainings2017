#include <iostream>

bool
isEven(int x)
{
    return x % 2 == 0;
}

int
main()
{
    int count;
    std::cout << "Enter the count of number:  ";
    std::cin >> count;
    std::cout << std::endl;

    for (int i = 1; i <= count; ++i) {
        int number;
        std::cout << "Enter a number to check: ";
        std::cin >> number;
       
        const bool result = isEven(number);
        if (result) {
            std::cout << number << "is even." << std::endl;
        } else {
            std::cout << number << "is odd." << std::endl;
        }
    
    }
    return 0;
}
