#include <iostream>

int
main()
{
    double sideA;
    double sideB;
    double sideC;

    std::cout << "Enter the first side of a triangle: ";
    std::cin >> sideA;
    if (sideA <= 0) {
        std::cerr << "Error 1: Triangle side can't be zero or negative." << std::endl;
        return 1;
    }
                        
    std::cout << "Enter the second side of a triangle: ";
    std::cin >> sideB;
    if (sideB <= 0) {
        std::cerr << "Error 1: Triangle side can't be zero or negative." << std::endl;
        return 1;
    }

    std::cout << "Enter the third side of a triangle: ";
    std::cin >> sideC;
    if (sideC <= 0) {
        std::cerr << "Error 1: Triangle side can't be zero or negative." << std::endl;
        return 1;
    }
   
    if (sideA > sideB) {
        if (sideA > sideC) {
            if (sideA * sideA == sideB * sideB + sideC * sideC) {
                std::cout << "Sides form a right-angled triangle." << std::endl;
                return 0;
            }
        }  
    } 

    if (sideB > sideA) {
        if (sideB > sideC) {
            if (sideB * sideB == sideA * sideA + sideC * sideC) {
                std::cout << "Sides form a right-angled triangle." << std::endl;
                return 0;
            }
        } 
    }

    if (sideC > sideA) {
        if (sideC > sideB) { 
            if (sideC * sideC == sideA * sideA + sideB * sideB) {
                std::cout << "Sides form a right-angled triangle." << std::endl;                                                                                             
                return 0;
            } 
        }
    }

    std::cout << "Sides can't form a right-angled triangle." << std::endl;
    return 0;
}

