#include <iostream>
#include <string>

int
main()
{
    std::string mode;
    std::cout << "Enter a mode (encode - encoding mode, decode - decoding mode): ";
    std::cin >> mode;

    if ("encode" == mode) {
        int number;

        std::cout << "Enter a number to encode: ";
        std::cin >> number;

        if (number < 1000) {
            std::cerr << "Error 1: Entered number doesn't contain 4 digits." << std::endl;
            return 1;
        }
        
        if (number > 9999) {
            std::cerr << "Error 1: Entered number doesn't contain 4 digits." << std::endl;
            return 1;
        }

        int digit1 = (number / 1000 + 7) % 10;
        int digit2 = (number / 100 + 7) % 10;
        int digit3 = (number / 10 + 7) % 10;
        int digit4 = (number / 1 + 7) % 10;

        std::cout << "Encoded number: " << digit3 << digit4 << digit1 << digit2 << std::endl;
        return 0;

    }

    if ("decode" == mode) {
        int number;

        std::cout << "Enter a number to decode: ";
        std::cin >> number;
    
        if (number < 1000) {
            std::cerr << "Error 1: Entered number doesn't contain 4 digits." << std::endl;
            return 1;
        }

        if (number > 9999) {
            std::cerr << "Error 1: Entered number doesn't contain 4 digits." << std::endl;
            return 1;
        }

        int digit1 = (number / 1000 + 3) % 10;
        int digit2 = (number / 100 + 3) % 10;
        int digit3 = (number / 10 + 3) % 10;
        int digit4 = (number / 1 + 3) % 10;    

        std::cout << "Decoded number: " << digit3 << digit4 << digit1 << digit2 << std::endl;
        return 0;
    }

    std::cerr << "Error 2: Input wasn't one of existed modes." << std::endl;
    return 2;
}

