#include <iostream>

int
main()
{
    while (true) {
        double  salesInDollars;

        std::cout << "Enter sales in dollars (-1 to exit): ";
        std::cin >> salesInDollars;

        if (-1 == salesInDollars) {
            break;
        }
       
        if (salesInDollars < 0) {
            std::cerr << "Error 1: Sales in dollars can't be negative." << std::endl;
            return 1;
        }

        std::cout << "Salary is: " << '$' << 200 + (salesInDollars / 100 * 9) << std::endl;
    }

    std::cout << std::endl;   
    return 0;
}

