#include <iostream>

int
main()
{
    int row = 1, width = 8;
    while (row < width) {
        int column = 1, length = 2 * width, moduloRow = row % 2;
        while (column < length) {
            if (column % 2 == moduloRow) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
            ++column;
        }
        ++row;
        std::cout << std::endl;
    }
    return 0;
}

