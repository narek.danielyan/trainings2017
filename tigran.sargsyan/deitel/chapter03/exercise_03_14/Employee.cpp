#include "Employee.hpp"

#include <iostream>
#include <string>

Employee::Employee(std::string name, std::string surname, int salary)
{
    setName(name);
    setSurname(surname);
    setSalary(salary);
}

void
Employee::setName(std::string name)
{
    name_ = name;
}

std::string
Employee::getName()
{
    return name_;
}

void
Employee::setSurname(std::string surname)
{
    surname_ = surname;
}

std::string
Employee::getSurname()
{
    return surname_;
}

void
Employee::setSalary(int salary)
{
    if (salary < 0) {
        std::cout << "Error 1. Salary is invalid. Resetting to 0." << std::endl;
        salary_ = 0;
        return;
    }

    salary_ = salary;
}

int
Employee::getSalary()
{
    return salary_;
}

