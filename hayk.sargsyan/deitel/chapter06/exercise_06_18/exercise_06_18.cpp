#include <iostream>
#include <assert.h>

int
integerPower(const int base, const int exponent)
{
    assert(exponent > 0);
    
    int pow = 1;
    for (int counter = 1; counter <= exponent; ++counter) {
        pow *= base;
    }

    return pow;
}

int
main()
{
    int base;
    
    std::cout << "Please enter the base: ";
    std::cin >> base;
    
    int exponent;
    
    std::cout << "Please enter the exponent: ";
    std::cin >> exponent;

    if (exponent < 1) {
        std::cerr << "Error 1: Exponent can't be zero or negative" << std::endl;
        return 1;
    }

    std::cout << integerPower(base, exponent) << std::endl;

    return 0;
}
