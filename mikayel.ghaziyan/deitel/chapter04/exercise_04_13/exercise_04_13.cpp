#include <iostream>
#include <iomanip>

int
main()
{
    while (true) {
        int miles = 0;
        std::cout << "Enter the miles used (Press -1 to quit): ";
        std::cin >> miles;

        if (-1 == miles) {
            return 0;    
        }

        if (miles < -1) {
            std::cerr << "Error 1: the miles canot have negative value." << std::endl;
            return 1;
        }

        int gallons; 
        std::cout << "Enter gallons used: ";
        std::cin >> gallons;
        if (gallons < 0) {
            std::cerr << "Error 2: The amount of gallons  cannot be a negative number." << std::endl;
            return 2;	
        }
        
        double tankful = static_cast<double>(miles) / gallons;

        int totalMiles = 0;
        totalMiles += miles;

        int totalGallons = 0;
        totalGallons += gallons;
        std::cout << "MPG this tankfull: " << std::setprecision(6) << std::fixed << tankful << std::endl;
        double total = static_cast<double>(totalMiles) / totalGallons;
        std::cout << "Total MPG: " << std::setprecision(6) << std::fixed << total << std::endl;
    } 
    return 0;
}
		  
