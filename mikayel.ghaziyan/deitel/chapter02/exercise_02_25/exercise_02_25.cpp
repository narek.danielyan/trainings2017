/// Mikayel Ghaziyan
/// 16/10/2017
/// Exercise e2.25

#include <iostream>

/// Beginning of the main
int
main() 
{
     /// Declarations
     int number1;
     int number2;

     /// Obtaining the numbers
     std::cout << "Please enter two numbers" << std::endl;
     std::cout << "Enter the first number: ";
     std::cin >> number1;
     std::cout << "Enter the second number: ";
     std::cin >> number2;

     /// Evaluating the inputs
     if (0 == number2) {
         std::cout << "Error 1. Division by zero. Try again" << std::endl;

	 return 1;
     }
     if (number1 % number2 == 0) {
         std::cout << number1 << " is multiple of " << number2 << std::endl;

	 return 0;
     }

     std::cout << number1 << " is not a multiple of " << number2 << std::endl;

     return 0;
} /// End of the main

/// End of the file

