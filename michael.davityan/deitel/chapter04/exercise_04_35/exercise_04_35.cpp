#include <iostream>
#include <unistd.h>

int
main()
{
    /// This is the (a) point of lesson....
    std::cout << "This is the (a) point of lesson....\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input number to calculate it's factorial: ";
    }
    int number;
    std::cin >> number;
    if (number < 0) {
        std::cout << "Error 1: Input number should be positive. " << std::endl;
        return 1;
    }
    std::cout << "The factorial is " << number << "!="; 
    int factorialOfNumber = 1;
    while (number > 1) {
        factorialOfNumber *= number;
        --number;
    }
    std::cout << factorialOfNumber << std::endl; 
    std::cout << "------------------------------------------------------------------------------------------------------\n";
    
    /// This is the (b) & (c) point of lesson....
    std::cout << "This is the (b) & (c) point of lesson....\n"; 
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the power of e: ";
    }
    int power;
    std::cin >> power;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the accuracy of the series e: ";
    }
    int accuracy;
    std::cin >> accuracy;
    if (accuracy < 0) {
        std::cout << "Error 1: accuracy value must be possitive." << std::endl;
        return 1;
    }
    int denominator = 1;
    double denominatorFactorial = 1.0;
    double eConstant = 1.0;
    double powerOfE = 1.0;
    int numerator = 1;
    while (denominator <= accuracy) {
        numerator *= power;
        denominatorFactorial *= denominator;
        eConstant += 1.0 / denominatorFactorial;
        powerOfE += numerator / denominatorFactorial;
        ++denominator;
    }
    std::cout << "e = " << eConstant << std::endl; 
    std::cout << "e to the power of " << power << " will be: "<< powerOfE << std::endl; 
    std::cout << "======================================================================================================" << std::endl;
    return 0;
}
