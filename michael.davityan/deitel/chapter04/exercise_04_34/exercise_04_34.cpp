#include <iostream>
#include <unistd.h>

int
main() 
{
    /// This is the (a) point of lesson....
    std::cout << "This is the (a) point of lesson....\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "insert a four-digit whole number: ";
    }
    int number;
    std::cin >> number;
    if (number < 1000) {
        std::cerr << "Error 1: the number must be a four-digit and positive." << std::endl;
        return 1;
    }
    if (number > 9999) {
        std::cerr << "Error 1: the number must be a four-digit and positive." << std::endl;
        return 1;
    }
    int encryption = 1000 * (((number % 100 / 10) + 7) % 10) + 100 * (((number % 10) + 7) % 10)
                     + 10 * (((number / 1000) + 7) % 10) + (((number % 1000 / 100) + 7) % 10);
    std::cout << "Encryption of your number is: "
              << encryption << std::endl; 
    std::cout << "------------------------------------------------------------------------------------------------------\n";

    /// This is the (b) point of lesson....
    std::cout << "This is the (b) point of lesson....\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert encryption(maximum four digit.): ";
    }
    int encryptedNumber;
    std::cin >> encryptedNumber;
    if (encryptedNumber < 0) {
        std::cerr << "Error 2: wrong encryption...\n" 
                  << "must be a positive number less than 10000.\n";
        return 2;
    } else if (encryptedNumber > 9999) {
        std::cerr << "Error 2: wrong encryption...\n" 
                  << "must be a positive number less than 10000.\n";
        return 2;
    }
    int initialNumber = 1000 * (((encryptedNumber % 100 / 10) + 10 - 7) % 10) 
                      + 100 * (((encryptedNumber % 10) + 10 - 7) % 10)
                      + 10 * (((encryptedNumber / 1000) + 10 - 7) % 10) 
                      + (((encryptedNumber % 1000 / 100) + 10 - 7) % 10);
    std::cout << "encrypted initial number is: "<< initialNumber << std::endl; 
    return 0;
}
