/* Question...
For each of the following, write a single statement that performs the specified task . Assume that longinteger variables value1 and value2 have been declared and value1 has been initialized to 200000.
a. Declare the variable longPtr to be a pointer to an object of type long.
b. Assign the address of variable value1 to pointer variable longPtr.
c. Print the value of the object pointed to by longPtr.
d. Assign the value of the object pointed to by longPtr to variable value2.
e. Print the value of value2.
f. Print the address of value1.
g. Print the address stored in longPtr. Is the value printed the same as value1's address? */

/// Answer....
#include <iostream>

int
main() {
/// for example....
    long value1 = 144169;
///----------------------  
    long value2;
/* a. */ long *longPtr;
/* b. */ longPtr = &value1;
/* c. */ std::cout << "longPtr pointer object value: " << *longPtr << std::endl;
/* d. */ value2 = *longPtr;
/* e. */ std::cout << "value2 variable value: " << value2 << std::endl;
/* f. */ std::cout << "value1 variable adress: " << &value1 << std::endl;
/* g. */ std::cout << "longPtr pointer variable value: " << longPtr << std::endl;
    std::cout << "And yes, its the same adress in f. and g. points....." << std::endl;

    return 0;
}
