Question of exercise!!!: State which of the following are true and which are false. If false, explain your answers.

a. C++ operators are evaluated from left to right.
b. The following are all valid variable names: _under_bar_ , m928134 , t5 , j7 , her_sales , his_account_total , a , b , c , z , z2 .
c. The statement cout << "a = 5;"; is a typical example of an assignment statement.
d. A valid C++ arithmetic expression with no parentheses is evaluated from left to right.
e. The following are all invalid variable names: 3g , 87 , 67h2 , h22 , 2h .

Answer!!!.

a. false. Not all C++ operators are evaluated from left to right, for example = operator is evaluated from right to left.
b. And true and false. Not all the identificators of variables are good. I see _under_bar_  variable identificator. C++ can use variables like this 
for its own purposes.
c.false. Its displays on the screen "a = 5" message.
d.thrue.
e.false. The variable name can not begin with a number but it can start with a letter. 
