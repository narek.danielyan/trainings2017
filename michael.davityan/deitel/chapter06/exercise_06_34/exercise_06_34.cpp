#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

bool flip();

int 
main() 
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    }

    int head = 0;
    int tail = 0;
    const int COIN_THROW_AMOUNT = 100;
    for (int count = 1; count <= COIN_THROW_AMOUNT; ++count) {
        if (flip()) {
            std::cout << std::setw(3) << count << "  Head" << std::endl;
            ++head;
        } else {
            std::cout << std::setw(3) << count << "  Tail" << std::endl;
            ++tail;
        }
    }
    std::cout << "During all throwing process....\n"
              << "Head: " << head << " times" << std::endl
              << "Tail: " << tail << " times" << std::endl;
    return 0;
}

bool 
flip()
{
    return static_cast<bool>(std::rand() % 2);
}


