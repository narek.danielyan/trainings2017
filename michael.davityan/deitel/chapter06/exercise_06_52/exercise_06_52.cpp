#include <iostream>
#include <cmath>
#include <unistd.h>

int 
main()
{
    std::cout << "----------------------------------------------------------------------------------------\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This is the work of the functions ceil().\n";
        std::cout << "Insert five different values\n";
    }
    const int VALUES_LIMIT = 5;
    for (int count = 1; count <= VALUES_LIMIT; ++count) {
        double ceilArgument;
        std::cin >> ceilArgument;
        std::cout << "ceil(" << ceilArgument << ")=" << std::ceil(ceilArgument) << std::endl;
    }
    std::cout << "----------------------------------------------------------------------------------------\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This is the work of the functions fabs().\n";
        std::cout << "Insert five different values\n";
    }
    for (int count = 1; count <= VALUES_LIMIT; ++count) {
        double fabsArgument;
        std::cin >> fabsArgument;
        std::cout << "fabs(" << fabsArgument << ")=" << std::fabs(fabsArgument) << std::endl;
    }
    std::cout << "----------------------------------------------------------------------------------------\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This is the work of the functions floor().\n";
        std::cout << "Insert five different values\n";
    }
    for (int count = 1; count <= VALUES_LIMIT; ++count) {
        double floorArgument;
        std::cin >> floorArgument;
        std::cout << "floor(" << floorArgument << ")=" << std::floor(floorArgument) << std::endl;
    }
    std::cout << "----------------------------------------------------------------------------------------\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This is the work of the functions sqrt().\n";
        std::cout << "Insert five different values\n";
    }
    for (int count = 1; count <= VALUES_LIMIT; ++count) {
        double sqrtArgument;
        std::cin >> sqrtArgument;
        std::cout << "sqrt(" << sqrtArgument << ")=" << std::sqrt(sqrtArgument) << std::endl;
    }
    std::cout << "----------------------------------------------------------------------------------------\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This is the work of the functions pow().\n";
        std::cout << "Insert five different values(base & exponent)\n";
    }
    for (int count = 1; count <= VALUES_LIMIT; ++count) {
        double powBase;
        double powExponent;
        std::cin >> powBase >> powExponent;
        std::cout << "pow(" << powBase << ", " << powExponent << ")=" 
                  << std::pow(powBase, powExponent) << std::endl;
    }
    std::cout << "----------------------------------------------------------------------------------------\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This is the work of the functions exp().\n";
        std::cout << "Insert five different values\n";
    }
    for (int count = 1; count <= VALUES_LIMIT; ++count) {
        double expArgument;
        std::cin >> expArgument;
        std::cout << "exp(" << expArgument << ")=" << std::exp(expArgument) << std::endl;
    }
    std::cout << "----------------------------------------------------------------------------------------\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This is the work of the functions log().\n";
        std::cout << "Insert five different values\n";
    }
    for (int count = 1; count <= VALUES_LIMIT; ++count) {
        double logArgument;
        std::cin >> logArgument;
        std::cout << "log(" << logArgument << ")=" << std::log(logArgument) << std::endl;
    }
    std::cout << "----------------------------------------------------------------------------------------\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This is the work of the functions log10().\n";
        std::cout << "Insert five different values\n";
    }
    for (int count = 1; count <= VALUES_LIMIT; ++count) {
        double log10Argument;
        std::cin >> log10Argument;
        std::cout << "log10(" << log10Argument << ")=" << std::log10(log10Argument) << std::endl;
    }
    std::cout << "----------------------------------------------------------------------------------------\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This is the work of the functions fmod().\n";
        std::cout << "Insert five different values(two numbers)\n";
    }
    for (int count = 1; count <= VALUES_LIMIT; ++count) {
        double fmodArgument1;
        double fmodArgument2;
        std::cin >> fmodArgument1 >> fmodArgument2;
        std::cout << "fmod(" << fmodArgument1 << ", " << fmodArgument2 << ")=" 
                  << std::fmod(fmodArgument1, fmodArgument2) << std::endl;
    }
    std::cout << "----------------------------------------------------------------------------------------\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This is the work of the functions sin().\n";
        std::cout << "Insert five different values\n";
    }
    for (int count = 1; count <= VALUES_LIMIT; ++count) {
        double sinArgument;
        std::cin >> sinArgument;
        std::cout << "sin(" << sinArgument << ")=" << std::sin(sinArgument) << std::endl;
    }
    std::cout << "----------------------------------------------------------------------------------------\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This is the work of the functions cos().\n";
        std::cout << "Insert five different values\n";
    }
    for (int count = 1; count <= VALUES_LIMIT; ++count) {
        double cosArgument;
        std::cin >> cosArgument;
        std::cout << "cos(" << cosArgument << ")=" << std::cos(cosArgument) << std::endl;
    }
    std::cout << "----------------------------------------------------------------------------------------\n";
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This is the work of the functions tan().\n";
        std::cout << "Insert five different values\n";
    }
    for (int count = 1; count <= VALUES_LIMIT; ++count) {
        double tanArgument;
        std::cin >> tanArgument;
        std::cout << "tan(" << tanArgument << ")=" << std::tan(tanArgument) << std::endl;
    }
    std::cout << "----------------------------------------------------------------------------------------\n";

    return 0;
}
